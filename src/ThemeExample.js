import React from "react";
import Theme from "./theme";
import styled from "styled-components";
import BreakpointExample from "./BreakpointExample";

const ThemeExample = () => {
  return (
    <Theme>
      <Container>
        <Heading isHeading={true}>Hello World</Heading>
        <h2>By the power of styled-components!</h2>
      </Container>
      <BreakpointExample />
    </Theme>
  );
};

export default ThemeExample;

const Container = styled.div`
  width: 100%;
  border: ${(props) => `1px solid ${props.theme.colors.onyx}`};
  background-color: ${(props) => props.theme.colors.lightBlue};
  font-family: ${(props) => props.theme.fonts[0]};
`;

const Heading = styled.h1`
  font-size: ${({ isHeading, theme: { fontSizes } }) =>
    isHeading ? fontSizes.large : fontSizes.small};
  color: ${({ theme: { colors } }) => colors.persianGreen};
`;

const Input = styled.input`
  // all your other inputFields styles
  @media (min-width: ${({
      theme: {
        breakPoints: { mobileS },
      },
    }) => mobileS}) {
    fontsize: ${({ theme: { fontSizes } }) => `${fontSizes[1]}px`};
  }
`;
