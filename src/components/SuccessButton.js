import styled from "styled-components";
import { Button } from "./Button";

export const SuccessButton = styled(Button)`
  background-color: #28a745;
  color: white;
`;
