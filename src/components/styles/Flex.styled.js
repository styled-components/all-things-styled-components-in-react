import styled from "styled-components";

export const Flex = styled.div`
  display: flex;
  align-items: center;

  // any direct div/ul set flex to 1, to set the columns
  & > div,
  & > ul {
    flex: 1;
  }
`;
