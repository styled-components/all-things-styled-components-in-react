import styled, { css } from "styled-components";
import { Button } from "./Button";

export const LargeButton = styled(Button)`
  background-color: #28a745;
  color: white;
  /* ${(props) =>
    props.fontSize &&
    css`
      font-size: ${props.fontSize};
    `} */

  /* font-size: ${({ fontSize }) => fontSize}; */
  font-size: ${({ theme }) => theme.fontLg};
`;
