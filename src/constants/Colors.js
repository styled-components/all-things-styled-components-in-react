const red = "red";
const green = "green";
const orange = "orange";

export default {
  red,
  green,
  orange,
};
