import React from "react";
import { Button } from "./components/Button";
import { LargeButton } from "./components/LargeButton";
import { SuccessButton } from "./components/SuccessButton";
import { ThemeProvider } from "styled-components";

const ExampleTwo = () => {
  return (
    <ThemeProvider theme={theme}>
      <Button primary>I am primary</Button>
      <Button>I am ordinary</Button>
      <SuccessButton>Success</SuccessButton>
      {/* <LargeButton fontSize={theme.fontLg}>Large</LargeButton> */}
      {/* now access font size props through the theme */}
      <LargeButton fontSize={theme.fontLg}>Large</LargeButton>
    </ThemeProvider>
  );
};

export default ExampleTwo;

const theme = {
  pd4: "4px",
  pd8: "8px",
  fontSm: "16px",
  fontMd: "18px",
  fontLg: "32px",
  primaryColor: "#28a745",
  textColor: "white",
};
