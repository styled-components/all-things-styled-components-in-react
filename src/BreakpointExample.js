import React from "react";
import styled from "styled-components";
import { device } from "./theme/device";

const BreakpointExample = styled.div`
  margin: auto;
  font-family: "sans-serif";
  text-align: center;

  @media ${device.laptop} {
    max-width: 800px;
  }

  @media ${device.desktop} {
    max-width: 1400px;
  }
`;

export default BreakpointExample;
