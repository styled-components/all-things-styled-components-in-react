import React from "react";
import ExampleOne from "./ExampleOne";
import ExampleTwo from "./ExampleTwo";
import SwitchStatement from "./SwitchStatement";
import ThemeExample from "./ThemeExample";

const App = () => {
  return (
    <div>
      {/* <ExampleOne /> */}
      {/* <ExampleTwo /> */}
      {/* <SwitchStatement /> */}
      <ThemeExample />
    </div>
  );
};

export default App;
