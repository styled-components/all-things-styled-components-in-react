import Colors from "../constants/Colors";

const renderColorType = (status) =>
  ({
    Processing: Colors.orange,
    New: Colors.orange,
    Cancelled: Colors.red,
    Ready: Colors.green,
    Delivered: Colors.green,
  }[status] ?? Colors.red);

// https://stackoverflow.com/questions/6114210/is-returning-out-of-a-switch-statement-considered-a-better-practice-than-using-b/65006217#65006217
// const renderColorType = (status) => {
//   switch (status) {
//     case "Processing":
//     case "New":
//       return Colors.orange;
//     case "Cancelled":
//       return Colors.red;
//     case "Ready":
//     case "Delivered":
//       return Colors.green;
//     default:
//       return Colors.red;
//   }
// };

export default renderColorType;
