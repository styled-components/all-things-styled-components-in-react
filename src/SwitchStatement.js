import React from "react";
import styled from "styled-components";
import renderColorType from "./utils/renderColor";

const DATA = [
  {
    id: 0,
    status: "Processing",
  },
  {
    id: 1,
    status: "New",
  },
  {
    id: 2,
    status: "Cancelled",
  },
  {
    id: 3,
    status: "Ready",
  },
  {
    id: 4,
    status: "Delivered",
  },
];

const SwitchStatement = () => {
  return (
    <div>
      {DATA.map(({ status }) => (
        <StatusTitle status={status} renderColorType={renderColorType}>
          {status}
        </StatusTitle>
      ))}
    </div>
  );
};

export default SwitchStatement;

const StatusTitle = styled.h2`
  color: ${({ status, renderColorType }) => renderColorType(status)};
`;
