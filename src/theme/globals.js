import { Platform } from "react-native";
import styled from "styled-components/native";
import Colors from "../constants/Colors";

// Families
export const FamilyMain = Platform.OS === "ios" ? "Roboto" : "sans-serif";
export const FamilyMedium =
  Platform.OS === "ios" ? "Roboto-Medium" : "sans-serif";
export const FamilyAlt =
  Platform.OS === "ios" ? "Roboto Condensed" : "sans-serif-condensed";

// Sizes
export const fontSizeMassive = 24;
export const fontSizeLarge = 18;
export const fontSizeStandard = 16;
export const fontSizeSmall = 14;
export const fontSize13 = 13;
export const fontSizeTiny = 12;
export const fontSizeSmallest = 11;
// export const headerFontSize = Layout.isSmallDevice ? fontSizeSmall : 16;

// Types
export const Text = styled.Text`
  font-family: ${({ medium }) => (medium ? FamilyMedium : FamilyMain)};
  color: ${({ color }) => color || Colors.fontMain};
  text-align: ${({ taLeft, taCenter }) =>
    taLeft ? "left" : taCenter ? "center" : "left"};
  font-size: ${({ fsLarge, fsSmall, fs }) =>
    fs ||
    (fsLarge ? fontSizeStandard : fsSmall ? fontSizeTiny : fontSizeSmall)}px;
  font-weight: ${({ bold, light, medium }) =>
    bold ? "bold" : light ? "300" : medium ? "500" : "normal"};
  margin-top: ${({ mt }) => mt || 0}px;
  margin-bottom: ${({ mb }) => mb || 0}px;
  margin-left: ${({ ml }) => ml || 0}px;
  margin-right: ${({ mr }) => mr || 0}px;
  ${({ flex }) => (flex ? "flex: 1" : "")}
  opacity: ${({ opacity }) => opacity || 1};
  text-decoration-line: ${({ underline }) =>
    underline ? "underline" : "none"};
`;

export const InputLabel = styled(Text)`
  text-align: left;
  color: ${({ color }) => color || Colors.fontMain};
  margin-bottom: ${({ marginBottom }) => (marginBottom ? 10 : 0)}px;
  margin-top: ${({ marginTop }) => (marginTop ? 20 : 0)}px;
  margin-left: ${({ marginLeft }) => (marginLeft ? 4 : 0)}px;
  padding-left: ${({ paddingLeft }) => (paddingLeft ? 4 : 0)}px;
  font-weight: ${({ bold, light }) =>
    bold ? "bold" : light ? "300" : "normal"};
`;

// Headings
export const CondensedHeading = styled(Text)`
  font-family: ${FamilyAlt};
  font-size: ${({ fs }) => fs || fontSizeLarge}px;
  font-weight: ${({ fwNormal }) => (fwNormal ? "normal" : "bold")};
  text-align: ${({ taCenter }) => (taCenter ? "center" : "left")};
  margin-bottom: ${({ mb }) => mb || 0}px;
`;

// Buttons
export const ButtonText = styled(Text)`
  font-size: ${({ fsSmall }) => (fsSmall ? fontSizeSmall : fontSizeStandard)}px;
  font-weight: bold;
  text-align: center;
  color: ${({ color }) => color || Colors.white};
`;
export const ButtonSecondaryText = styled(ButtonText)`
  color: ${Colors.fontMain};
`;

export const Button = styled.Pressable`
  background-color: ${({
    secondary,
    borderButton,
    disabled,
    whiteButton,
    successButton,
    bg,
  }) =>
    secondary
      ? Colors.fontMain
      : borderButton
      ? "transparent"
      : disabled
      ? Colors.cefRedInactive
      : whiteButton
      ? Colors.white
      : successButton
      ? Colors.success
      : bg || Colors.cefRed};
  height: ${({ height }) => height || 47}px;
  width: ${({ width }) => width || 100}%;
  margin-top: ${({ mt }) => (typeof mt !== "undefined" ? mt : 10)}px;
  margin-left: ${({ ml }) => ml || 0}px;
  margin-right: ${({ mr }) => mr || 0}px;
  margin-bottom: ${({ mb }) => mb || 10}px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  ${({ borderButton }) =>
    borderButton
      ? {
          borderWidth: 2,
          borderColor: Colors.fontMain,
        }
      : ""}
  ${({ flexValue }) => (flexValue ? { flex: flexValue } : "")};
`;

export const Btn = ({
  onPress,
  text,
  secondary,
  borderButton,
  icon,
  ml,
  mr,
  mt,
  mb,
  height,
  columns,
  flex: flexValue,
  disabled,
  whiteButton,
  successButton,
  bg,
  fsSmall,
}) => (
  <Button
    onPress={onPress}
    disabled={disabled}
    secondary={secondary}
    borderButton={borderButton}
    whiteButton={whiteButton}
    successButton={successButton}
    bg={bg}
    ml={ml}
    mr={mr}
    mt={mt}
    mb={mb}
    height={height}
    width={columns ? (1 / columns) * 100 : 100}
    flexValue={
      flexValue && Number.isInteger(flexValue)
        ? flexValue
        : flexValue
        ? 1
        : false
    }
  >
    {icon && icon}
    <ButtonText
      fsSmall={fsSmall}
      ml={icon ? 10 : 0}
      color={
        borderButton
          ? Colors.fontMain
          : whiteButton
          ? Colors.cefRed
          : Colors.white
      }
    >
      {text}
    </ButtonText>
  </Button>
);
