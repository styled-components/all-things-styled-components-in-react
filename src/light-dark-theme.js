import React, { useState, useEffect } from "react";
import styled, { ThemeProvider } from "styled-components";

const themeDefault = {
  id: "dark",
  colorPrimary: "rebeccapurple",
  colorText: "white",
};

const themeDark = {
  colorPrimary: "rebeccapurple",
  colorBackground: "black",
  colorText: "white",
};

const themeLight = {
  id: "light",
  colorBackground: "white",
  colorText: "black",
};

const StyledButton = styled.button`
  background: ${(props) => props.theme.colorPrimary};
  color: white;
  /* background: ${(props) => (props.isActive ? "red" : " blue")}; */
`;

const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: ${(props) => props.theme.colorBackground};
  color: ${(props) => props.theme.colorText};
  transition: background 0.35s;
`;

function App() {
  const newTheme = { ...themeDefault, ...themeLight };

  const handleThemeToggled = () => {
    // if theme is dark it will set it to light theme, otherwise set it to the dark theme
    if (theme.id === "dark") {
      setTheme({
        ...themeDefault,
        ...themeLight,
      });
    } else {
      setTheme({
        ...themeDefault,
        ...themeDark,
      });
    }
  };

  const [theme, setTheme] = useState(themeDefault);

  useEffect(() => {
    setTheme({
      ...themeDefault,
      ...themeLight,
    });
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <StyledWrapper>
        <StyledButton isActive onClick={handleThemeToggled}>
          Toggle Theme
        </StyledButton>
      </StyledWrapper>
    </ThemeProvider>
  );
}

export default App;
