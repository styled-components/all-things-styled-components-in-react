## Styled components

- Allows us to write css in a very elegant and reusable way using css in js

- If you want to encapsulate your components and have everything in one place its a great option

- One of the main uses of styled components is to not use classes. If elements dont need them(we can just use nested tags such as h1{}) then we can just leave them alone and just add a styled component wrapper for a lot of cases

- How styled components works is it takes all the styles:
  const Button = styled.button`...` and applies a random class

1. ExampleOne is from brad traversy on using styled components
2. ExampleTwo is using more advanced concepts - extending and passing props and basic theming
3. ExampleThree - advanced theming and props

- Theme folder includes using breakpoints and creating global text/button components
